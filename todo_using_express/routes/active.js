const express = require('express');
const router = express.Router();
const ToDoDB = require('../Tododb');
const filterType = 'active';

router.get('/', async(req, res) => {
    const filterType = 'active';
    const data = await ToDoDB.find({isCompleted : false});
    const count = await ToDoDB.find({isCompleted : false}).count();
    res.render('index.ejs', {data : data, filterType : filterType, count : count});
})

router.post('/add', async(req, res) => {
    try {
        const newTodo = await ToDoDB.create({
            todo : req.body.todo,
            isCompleted: false
        })
        newTodo.save();
    } catch (error) {
        console.log(error.message);
    }
    res.redirect('/active');
})

router.get('/strike/:id', async(req, res) => {
    const bool = (await ToDoDB.findById(req.params.id)).isCompleted;
    await ToDoDB.updateOne({_id : req.params.id}, { $set : { isCompleted : !bool }});
    res.redirect('/active');
}) 

router.get('/delete/:id', async(req, res) => {
    await ToDoDB.deleteOne({_id : req.params.id});
    res.redirect('/active');
})

router.get('/clearcompleted', async(req, res) => {
    await ToDoDB.deleteMany({isCompleted : true});
    res.redirect('/active');
})

module.exports = router;
