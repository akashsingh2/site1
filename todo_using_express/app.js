const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 4080;
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const ToDoDB = require('./Tododb');
const allRouter = require('./routes/all');
const activeRouter = require('./routes/active');
const completeRouter = require('./routes/completed');

dotenv.config();

app.set("view engine", "ejs");

app.use(express.static('public'));
app.use('css', express.static(__dirname + 'public/css'));
app.use(bodyParser.urlencoded({extended : true}));
app.use('/all', allRouter);
app.use('/active', activeRouter);
app.use('/complete', completeRouter);

mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(()=>console.log("DB Connection Success"))
.catch((err) => console.log(err));

app.get('/', async(req, res) => {
     res.redirect('/all');
    // res.send(JSON.stringify(await ToDoDB.find({})));
})

app.listen(PORT, ()=> {console.log(`Listening on PORT ${PORT}`);
})
